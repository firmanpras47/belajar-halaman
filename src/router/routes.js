
const routes = [
  {
    path: '/',
    component: () => import('layouts/shop/sabtulay.vue'),
    children: [
      { path: 'sabtuku', component: () => import('pages/sabtuku.vue') },
      { path: 'Halaman2', component: () => import('pages/Halaman2.vue') },
      { path: 'Halaman3', component: () => import('pages/Halaman3.vue') },
      { path: 'Halaman4', component: () => import('pages/Halaman4.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
